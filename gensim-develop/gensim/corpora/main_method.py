__author__ = "McKenzie"
from gensim import utils
from gensim.corpora import TextCorpus
from six import *
import sys
from cStringIO import StringIO
import os

def main():
    """
    Runs the textcorpus.py file to generate data to a list
    of word ids
    """
    files_list = []
    for root, dirs, files in os.walk('/Users/mckenzie/Desktop/kdd/add_to_git/gensim-develop'):
        for x in files:
            #print root + '/' + x
            files_list.append(root + '/' + x)
    print(files_list)
    #Filter through the files uploaded and just keep the '.txt' files
    files_list = list(filter(lambda j: j[-4:] == '.txt', files_list))
    for f in files_list:
        print(f)

    fname = "text.txt"

    file_ = open("text.txt", "r")
    oneLine = ''
    for line in file_.readlines():
        oneLine = oneLine+'\n'+line.replace("\'", "")
    data = TextCorpus(StringIO(oneLine))
    print data.dictionary
    #for key in data.dictionary.values():
     #   print key
    file_.close()

    oneLine = ''
    a_file = open("textAppend.txt", "r")
    for line in a_file.readlines():
        oneLine = oneLine+'\n'+line.replace("\'", "")
    list_a = [oneLine.lower().split()]
    data.dictionary.add_documents(list_a)
    for key in data.dictionary.values():
       print key
    #for key in data2.dictionary.values():
    #    print key
    #data.dictionary.add_documents(newFile)
    #with open('IDs.txt', 'w') as file_:
        #print(word_id_list)

main()