__author__ = "McKenzie"
from gensim import utils
from gensim.corpora import TextCorpus
from six import *
import sys
from cStringIO import StringIO
import os
import numbers

def main():
    """
    Runs the textcorpus.py file to generate data to a list
    of word ids
    """
    files_list = []
    for root, dirs, files in os.walk('/Users/mckenzie/Desktop/kdd/add_to_git/gensim-develop'):
        for x in files:
            #print root + '/' + x
            files_list.append(root + '/' + x)

    #Filter through the files uploaded and just keep the '.txt' files
    files_list = list(filter(lambda j: j[-4:] == '.txt', files_list))
    for f in files_list:
        print(f)

    data = TextCorpus()

    for file in files_list:
        a_file = open(file, 'r')
        one_line = ''
        for line in a_file.readlines():
            one_line = one_line + '\n' + line.replace('\'', '')
        a_list = [one_line.lower().split()]
        #a_list = list(filter(lambda b: not isinstance(b, (int, long, float, complex)), a_list))
        #print a_list
        #a_list = list(filter(lambda b: b.isalpha(), a_list))
        b_list = []
        for i in range(len(a_list)):
            for j in range(len(a_list[i])):
                if not isinstance(a_list[i][j], (int, long, float, complex)):
                    b_list.append(a_list[i][j])
        #print b_list

        data.dictionary.add_documents(a_list)
        a_file.close()

def try_int(x):
    try:
        return int(x)
    except:
        return x

main()